#!/bin/python3
import traceback
from transmission_rpc import Client
from transmission_rpc.torrent import Torrent
from pprint import pprint
from getpass import getpass
from time import sleep
import log
import logging, sys, curses, os, asyncio
from curses import wrapper
from curses.textpad import rectangle, Textbox
import curses.panel
from signal import signal, SIGWINCH, SIGINT
import locale
locale.setlocale(locale.LC_ALL, "")

from popup import PopUp

DEBUG = True

def getParams():
    print(len(sys.argv))
    global HOST
    global PORT
    global username
    global password
    if len(sys.argv) == 2:
        HOST = sys.argv[1]
        PORT = 9091
    elif len(sys.argv) == 3:
        PORT = sys.argv[2]
    else:
        HOST = "localhost"
        PORT = 9091
    print(f"{HOST}:{PORT}")
    username = input("Username: ")
    try:
        with open('pwd.lol', 'r') as f:
            password = f.read()
    except Exception as e:
        password=getpass("Pw> ")
    res = input("Save parameters? (y/n)> ")
    if res == 'y':
        with open('pwd.lol', 'w') as f:
            f.write(f"{username}:{password}:{HOST}:{PORT}")
    else:
        print("Not saving")

if os.path.isfile('pwd.lol'):
    if len(sys.argv) >= 3:
        os.remove('pwd.lol')
        print("Removed pwd.lol\nExiting...")
        exit(0)
    global HOST
    global PORT
    global username
    global password
    with open('pwd.lol', 'r') as f:
        username, password, HOST, PORT = f.read().split(':')
else:
    getParams()
try:
    trans_rpc = logging.getLogger('transmission_rpc')
    trans_rpc.setLevel(logging.WARN)
    client = Client(host=HOST, port=PORT, username=username, password=password, logger=trans_rpc)
except Exception as e:
    logging.error(str(e))
    logging.error("Failed to connect to transmission-daemon")
    print("Check if you passwd is correct or if transmission-daemon is running")
    raise e

def bytes2human(n):
    symbols = ('K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y')
    prefix = {}
    for i, s in enumerate(symbols):
        prefix[s] = 1 << (i+1)*10
    for s in reversed(symbols):
        if n >= prefix[s]:
            value = float(n) / prefix[s]
            return '%.1f%s' % (value, s)
    return "%sB" % n

def main(stdscr:curses.window):
    stdscr.clear()
    curses.init_pair(3, curses.COLOR_BLACK, curses.COLOR_WHITE)
    height, width = stdscr.getmaxyx()
    stdscr.nodelay(1)
    curses.curs_set(False)
    key = -1
    stdscr.clear()
    stdscr.addstr(0, 0, f"Transmission-RPC-CLI on {HOST}:{PORT}")
    stdscr.addstr(1, 0, "Press Ctrl-C to exit")
    stdscr.addstr(3, 1, "Name")
    stdscr.addstr(3, 61, "Size")
    stdscr.addstr(3, 81, "Status")
    stdscr.addstr(3, 101, "Progress in %")
    stdscr.addstr(3, 121, "Ratio")
    stdscr.refresh()
    try:
        logging.info("Trying to the rectangle to been drawn")
        rectangle(stdscr, 2, 0, 5, width-1)
    except Exception as e:
        logging.error(str(e))
        logging.error("Possibly a bug in curses.rectangle() when calling on statusWin")
    stdscr.refresh()
    sleep(0.005)
    #ch=stdscr.getch()
    torwin = curses.newwin(height-4, width, 4, 0)
    torwinH, torwinW = torwin.getmaxyx()
    def drawTorWin():
        try:
            rectangle(torwin, 0, 0, torwinH-1, torwinW-1)
        except Exception as e:
            logging.error(str(e))
            logging.error("Possibly a bug in curses.rectangle() when calling on torwin")
    drawTorWin()
    logging.info("torWin started")
    torwinH, torwinW = torwin.getmaxyx()
    torwin.nodelay(True)
        
    #TORWIN SHIT FEST
    LNT = 0
    def calcLineLen(*lines):
        length = [len(x) for x in lines]
        sumLen = sum(length)
        with open("lol.tyt","w") as f:
            for len_ in length:
                f.write(str(len_)+"\n")
            f.write(str(sumLen)+"\n")

    def writeToTorwin(torrent:Torrent, i:int,stantout:bool=False):
        calcLineLen(torrent.name, bytes2human(torrent.totalSize), str(torrent.status), str(torrent.ratio))
        if stantout:
            torwin.standout()
        #torwin.addstr(i, 1, f"{torrent.name}\t{bytes2human(torrent.totalSize)}\t{torrent.status}\t{torrent.percentDone*100}%\t{torrent.ratio}")
        torwin.addstr(i, 1, torrent.name)
        torwin.addstr("\t"*15)
        torwin.addstr(i, 61, bytes2human(torrent.total_size))
        torwin.addstr(i, 81, torrent.status)
        torwin.addstr(i, 101, str(torrent.progress))
        torwin.addstr(i, 121, str(torrent.ratio))
        if stantout:
            torwin.standend()
        torwin.refresh()

    def drawStatus(status:str):
        torwin.addstr(torwinH-1, 3, status)
        torwin.refresh()
        if "Seeding" not in status:
            logging.info(f"Status: {status}")

    cur = 1
    while key != ord('q'):
        torrents = client.get_torrents()
        tors = len(torrents)
        session = client.get_session()
        status = f"TorWin: {torwinH}x{torwinW} on {HOST}:{PORT} | Seeding {tors} torrents | Free space of '/' {bytes2human(client.free_space(path='/'))}"
        key = torwin.getch()
        if key == ord('a'):
            logging.info("Adding file ...")
            logging.info(f"torwinw/2 {int(torwinW/2), int(torwinH/2)}")
            popupx, popupy = 52,12
            title = "Choose adding method"
            status_ = f"TorWin: {torwinH}x{torwinW} on {HOST}:{PORT} | Adding file mode" 
            status_ += "─"*(len(status)-len(status_))
            drawStatus(status_)
            x = PopUp(stdscr, title, popupx, popupy, int(torwinW/2-(popupx/2)), int(torwinH/2-(popupy/2)),int((popupx-len(title))/2),0, border=True, autoDraw=True, highLightTitle=False)
            x.addLine("1. (M)agnet link ",3,-1)
            x.addLine("2. (T)orrent file", 6, -1)
            x.addLine("3. (Q)uit", 9, -1)
            def onExit():
                x.addLine("3. (Q)uit", 9, -1, highLighted=True)
                x.functions["run"] = False
                torwin.clear()
                drawTorWin()
                drawStatus(status)                
            def onM():
                logging.info("Magnet link chosen")
                mtitle = "Enter magnet link:"+" "*(len(title)-len("Enter magnet link:"))
                x.title = mtitle
                x.draw(titleRun=True)
                x.clear()
            x.onKey(['q', 27], onExit)
            x.onKey(['m', 49], onM)
            x.keyHook()
            del x
            logging.info("Added file")
        if key == curses.KEY_DOWN or key == 258 or key == ord('j'):
            logging.info("Down pressed")
            if tors > cur:
                cur += 1
        if key == curses.KEY_UP or key == 259 or key == ord('k'):
            logging.info("Up pressed")
            if cur != 1:
                cur -= 1
        i = 1        
        for torrent in torrents:
            if len(torrent.name) > LNT: LNT = len(torrent.name)
            if cur == i:
                writeToTorwin(torrent, i, True)
            else:
                writeToTorwin(torrent, i)
            i += 1
        drawStatus(status=status)
    logging.info("torWin exiting")
    return
        
def resizeHandler(signum, frame):
    curses.endwin()
    curses.initscr()
    curses.doupdate()
    curses.noecho()
    curses.cbreak()
    curses.curs_set(0)
    curses.start_color()
    curses.use_default_colors()
    curses.init_pair(3, curses.COLOR_BLACK, curses.COLOR_WHITE)
    main(curses.initscr())

def intHandler(signum, frame):
    curses.endwin()
    curses.echo()
    curses.nocbreak()
    curses.curs_set(1)
    logging.info("intHandler exiting")
    exit(0)

if __name__ == '__main__':
    signal(SIGWINCH, resizeHandler)
    signal(SIGINT, intHandler)
    if DEBUG:
        import tracemalloc
        tracemalloc.start()
    try:
        ESCDELAY = 25
        wrapper(main)
    except (SystemExit, KeyboardInterrupt):
        curses.endwin()        
        sys.exit(0)
    except Exception as e :
        curses.endwin()
        curses.echo()
        curses.nocbreak()
        curses.curs_set(1)
        summary = traceback.StackSummary.extract(traceback.walk_stack(None))
        with open("log.txt_", "w") as f:
            f.write(''.join(summary.format()))
            f.write('\n')
        #logging.ERROR(msg=f"{summary}")
        print(summary)
        raise e