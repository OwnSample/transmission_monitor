import curses
import logging
from curses.textpad import rectangle
from erros import *

class PopUp():
    def __init__(self, window:curses.window, title:str, width:int, height:int, x:int, y:int, tx:int, ty:int, border:bool=True, autoDraw:bool=True, highLightTitle:bool=True, lines:list={"lines":[]}):
        self.window = window
        self.title = title
        self.width = width
        self.height = height
        self.x = x
        self.y = y
        self.tx = tx
        self.ty = ty
        #self.border = border
        self.lines = lines
        self.borderDrawn = False
        self.functions = {"actions" : [], "run": True}
        logging.info(msg=f"PopUp created: {self.title}")
        if autoDraw:
            self.draw(border=border, highLightTitle=highLightTitle, titleRun=True)

    def __del__(self):
        logging.info(msg=f"PopUp deleted: {self.title}")

    def addLine(self, text:str, y:int, x:int, highLighted:bool=False, refresh:bool=True):
        if x == -1:
            x = int((self.x-len(text))/5) # try to self-center
        self.lines["lines"].append({"text": text, "y": y, "x": x, "highLighted": highLighted})
        if refresh:
            self.draw()

    def onKey(self, key:str|int, func):
        if not key and not func:
            raise EmptyFunctionError("No function and key given")
        self.functions["actions"].append({"key" : key, "func" : func})
        logging.info(msg=f"PopUp keyHook action added: {self.title} {func} to {key}")
        logging.debug(msg=f"PopUp keyHook actions: {self.title} {self.functions['actions']}")

    def onKey(self, keys:list[str|int], func):
        if not keys and not func:
            raise EmptyFunctionError("No function and key given")
        for key in keys:
            self.functions["actions"].append({"key" : key, "func" : func})
            logging.info(msg=f"PopUp keyHook action added: {self.title} {func} to {key}")
        logging.debug(msg=f"PopUp keyHook actions: {self.title} {self.functions['actions']}")

    def keyHook(self, *,noKey:bool=False):
        logging.info(msg=f"PopUp keyHook started: {self.title}")
        def run(self):
            while self.functions["run"]:
                key = self.window.getch()
                if key == -1:
                    continue
                for x in self.functions["actions"]:
                    if chr(key) == x["key"] or key == x["key"]:
                        x["func"]()
                        logging.info(msg=f"PopUp keyHook action run: {self.title} {x['func']}")
        if self.functions != {"actions" : [], "run": True}:
            logging.info(msg=f"PopUp keyHook running: {self.title} with {self.functions['actions']}")
            run(self)
        elif noKey:
            run(self)
            logging.warn(msg=f"PopUp keyHook no actions: {self.title}")

    def clear(self, border:bool=False, titleRun:bool=False, highLightTitle:bool=False):
        if border:
            rectangle(self.window, self.y, self.x, self.y+self.height, self.x+self.width)
            self.borderDrawn = True
            logging.info(msg=f"PopUp border cleared: {self.title}")
        if titleRun:
            if highLightTitle:
                self.window.standout()
            self.window.addstr(self.ty+self.y, self.x+self.tx, self.title)
            if highLightTitle:
                self.window.standend()
            logging.info(msg=f"PopUp title cleared: {self.title}")
        for line in self.lines["lines"]:
            self.window.addstr(line["y"]+self.y, line["x"]+self.x, " "*len(line["text"]))
            logging.info(msg=f"PopUp line cleared: {self.title} {line}")
        self.window.refresh()
        logging.info(msg=f"PopUp cleared: {self.title}")

    def draw(self, border:bool=False, titleRun:bool=False, highLightTitle:bool=False):
        if border and not self.borderDrawn:
            rectangle(self.window, self.y, self.x, self.y+self.height, self.x+self.width)
            self.borderDrawn = True
            logging.info(msg=f"PopUp border drawn: {self.title}")
        if titleRun:
            if highLightTitle:
                self.window.standout()
            self.window.addstr(self.ty+self.y, self.x+self.tx, self.title)
            if highLightTitle:
                self.window.standend()
            logging.info(msg=f"PopUp title run: {self.title}")
        for line in self.lines["lines"]:
            if line["highLighted"]:
                self.window.standout()
            self.window.addstr(line["y"]+self.y, line["x"]+self.x, line["text"])
            if line["highLighted"]:
                self.window.standend()
            logging.info(msg=f"PopUp line run: {self.title} {line}")
        self.window.refresh()
        logging.info(msg=f"PopUp drawn: {self.title}")
