import logging, os
from datetime import date

class _Format(logging.Formatter):
    def format(self, record):
        filename = record.filename
        a = "%s:%s" % (filename, record.lineno)
        return "[%s] %s" % (a, record.msg)

if not os.path.exists("log"):
    os.mkdir("log")

ch = logging.StreamHandler()
#ch.setLevel(logging.WARNING)
ch.setFormatter(_Format())
logging.basicConfig(filename=f'log/{date.today()}.log', filemode='w', level=logging.INFO,
                    format='%(asctime)s-%(name)s-%(filename)s:%(lineno)s %(levelname)s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
